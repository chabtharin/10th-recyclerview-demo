package com.chabtharin.practicerecyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.MyHolder> {
    private ArrayList<CarModel> carModels;

    public CarAdapter(ArrayList<CarModel> carModels) {
        this.carModels = carModels;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        CarModel carModel = carModels.get(position);
        holder.title.setText(carModel.getTitle());
    }

    @Override
    public int getItemCount() {
        return carModels.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder{
        private TextView title;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.text_title);
        }
    }
}
