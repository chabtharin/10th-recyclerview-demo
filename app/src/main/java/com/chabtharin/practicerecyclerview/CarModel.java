package com.chabtharin.practicerecyclerview;

public class CarModel {
    private String title;

    public CarModel(String title) {
        this.title = title;
    }

    public CarModel() {

    }

    public String getTitle() {
        return title;
    }
}
