package com.chabtharin.practicerecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ArrayList<CarModel> carModels = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler);
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        carModels.add(new CarModel("Car1"));
        CarAdapter carAdapter = new CarAdapter(carModels);
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);
        recyclerView.setAdapter(carAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }
}